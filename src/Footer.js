import React from 'react'
import './Footer.css'
function Footer() {
  return (
    <div className="footer">
      <p><strong>2020</strong>. <a href="https://instagram.com/iim_mafahir" target="_blank">iimmafahir</a>.</p>
    </div>
  )
}

export default Footer
